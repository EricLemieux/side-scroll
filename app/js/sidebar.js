var canvas;
var ctx;

//Stop complaining if things arent loaded yet
loaded = false;

//Scroll position used to determine direction
var lastScrollPosition

//Images used for rendering the cube
var images = [];

//Temp vars for testing
var x = 0;
var y = 0;
var w = 50;
var h = 50;

//Init the canvas and make sure we can draw on it properly
function onLoad() {
    canvas = document.getElementById('sidebar');
    ctx = canvas.getContext('2d');

    loaded = true;

    canvasResize();

    //Populate the images array
    images = document.querySelectorAll('.canvas-images img');

    //Set the temp vars to be in the middle of the screen
    w = images[0].width;
    h = images[0].height;
    x = canvas.width/2 - w/2;
    y = canvas.height/2 - h/2;

    //Paint the first frame
    paint();
}

//Set the dimentions of the canvas to match the size of the window we want to use
function canvasResize() {
    if(!loaded)
        return;

    canvas.width = window.innerWidth/2;
    canvas.height = window.innerHeight;
}

function paint(activeElement = 0, offset = 0) {
    if(!loaded)
        return;
        
    //Clear the frame buffer
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    var prevElement = activeElement <= 0 ? images.length - 1 : activeElement - 1;
    var nextElement = activeElement >= images.length - 1 ? 0 : activeElement + 1;;

    //Draw the images
    //Only need to draw the active element and the elements beside it because images are going to be large
    ctx.drawImage(images[activeElement], 0 + offset, y);

    //Previous element
    ctx.drawImage(images[prevElement], -w + offset, y);

    //Next element
    ctx.drawImage(images[nextElement], w + offset, y);
}

function pageScroll() {
    if(!loaded)
        return;
        
    if (lastScrollPosition > window.scrollY) { //Scrolling down
        x += 25;
    }else { //Scrolling up
        x -= 25;
    }
    lastScrollPosition = window.scrollY;

    var diff = window.scrollY%(images.length*w);
    var activeElement = Math.floor(diff/w);

    console.log(activeElement);

    paint(activeElement,diff);
}