var gulp = require('gulp');
var browserSync = require('browser-sync');
var sass = require('gulp-sass');
var reload = browserSync.reload;

gulp.task('build-css', function(){
    return gulp.src('app/sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(reload({stream: true}));
});

gulp.task('hard-reload', function() {
    return reload();
});

gulp.task('default', ['build-css'], function(){
    browserSync({
        server: {
            baseDir: 'app'
        }
    });

    gulp.watch(['app/**/*.scss'], ['build-css']);
    gulp.watch(['app/**/*.html', 'app/**/*.js'], ['hard-reload']);
});